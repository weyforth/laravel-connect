<?php
/**
 * Connect driver interface.
 *
 * Connect drivers must implement this interface.
 * Provides the connection manager with all the information
 * and methods required to connnect to the disired social
 * network and execute common commands such as posting a
 * message, getting user information etc.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Connect;

interface DriverInterface
{


    /**
     * Returns the url used to connect app with protocol.
     *
     * @param boolean $authenticate Whether to begin a new
     * authenticate request instead of an authorize request.
     *
     * @return string
     */
    public function connectURL($authenticate = true);


    /**
     * Callback function for connect, called on return to site.
     *
     * @return void
     */
    public function respond();


    /**
     * Attempt to logout with driver and reconnect using different credentials.
     *
     * @return void
     */
    public function reconnect();


    /**
     * Disconnects app from protocol, destroy sessions and redirect.
     *
     * @return void
     */
    public function disconnect();


    /**
     * Whether the protocol is being actively used, should not tax the api.
     *
     * @return boolean
     */
    public function isActive();


    /**
     * User has correct and valid permission.
     *
     * Called before making taxing request to api to check whether user has
     * required permissions to make a post. Provides more robust check than
     * isActive(), and can tax the api. Also provides the api with the user
     * token and secret needed to make authorized calls.
     *
     * @return boolean
     */
    public function hasPermissions();


    /**
     * Posts message to social network.
     * 
     * Returns and array containing to id of the post
     * and the id of the user on success, false on failure.
     *
     * @param string $message Message string to post.
     *
     * @return array or false
     */
    public function postMessage($message);


    /**
     * Removes the specififed message from the social network.
     *
     * Usally called after an error after the message has been posted,
     * if for example the post fails to save to the database.
     *
     * @param string $id ID of message to remove.
     *
     * @return boolean
     */
    public function removeMessage($id);


    /**
     * Follows the specified user/account.
     *
     * @param string $info Info hash of user/account to follow.
     *
     * @return void
     */
    public function follow($info);


    /**
     * Retrieves the profile image src for the currently logged in user.
     *
     * @return string
     */
    public function userImage();


    /**
     * Retrieves the id of the currently logged in user.
     *
     * @return string
     */
    public function userID();


    /**
     * Retrieves the screen name of the currently logged in user.
     *
     * @param boolean $extra Whether to include additional characters
     * to the users screen name eg '@' or '/'.
     *
     * @return string
     */
    public function userScreenName($extra = true);


    /**
     * Retrieves the full name of the currently logged in user.
     *
     * @return string
     */
    public function userName();


    /**
     * Retrieves the icon for the service.
     *
     * @return string
     */
    public function icon();


    /**
     * Retrieves the post message adjective for the protocol.
     *
     * @return string
     */
    public function verb();


}
