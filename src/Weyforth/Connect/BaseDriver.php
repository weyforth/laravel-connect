<?php
/**
 * Base driver.
 *
 * Connect drivers can extend this class to provide
 * session storage unique to the connecting driver.
 * Useful for storing tokens between requests, user
 * information etc.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Connect;

use Illuminate\Support\Facades\Session;

class BaseDriver
{

    /**
     * The session key, unique to each extended class.
     *
     * @var string $sessionKey
     */
    protected $sessionKey;


    /**
     * Constructor sets session key from class name.
     */
    public function __construct()
    {
        $this->sessionKey = str_replace('\\', '.', get_class($this));
    }


    /**
     * Sets data to the class session.
     *
     * @param object $data Data to store in the session.
     *
     * @return void
     */
    public function setSessionData($data)
    {
        Session::put($this->sessionKey, $data);
    }


    /**
     * Retrieves all class session data.
     *
     * @return object
     */
    public function getSessionData()
    {
        return Session::get($this->sessionKey);
    }


    /**
     * Unsets all class session data.
     *
     * @return void
     */
    public function unsetSessionData()
    {
        Session::forget($this->sessionKey);
    }


}
