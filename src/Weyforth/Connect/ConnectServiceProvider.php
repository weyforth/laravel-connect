<?php
/**
 * Connect Service Provider.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Connect;

use Illuminate\Support\ServiceProvider;

class ConnectServiceProvider extends ServiceProvider
{

    /**
     * Defer loading of the provider.
     *
     * @var boolean
     */
    protected $defer = true;


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared(
            'connect',
            function ($app) {
                return new ConnectManager($app);
            }
        );

        $this->app->bindShared(
            'connect.store',
            function ($app) {
                return $app['connect']->driver();
            }
        );

        $this->app['config']['connect.driver'] = 'facebook';
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('connect', 'connect.connector');
    }


    /**
     * Get the name of the current registered driver.
     *
     * @return string
     */
    protected function getDriver()
    {
        return $this->app['config']['connect.driver'];
    }


}
