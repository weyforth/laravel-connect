<?php
/**
 * Social connection Laravel extension, with Facebook & Twitter Support.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Connect;

use Illuminate\Support\Manager;

class ConnectManager extends Manager
{


    /**
     * Create an instance of the Facebook driver.
     *
     * @return Weyforth\Connect\FacebookDriver
     */
    protected function createFacebookDriver()
    {
        return new FacebookDriver;
    }


    /**
     * Create an instance of the Twitter driver.
     *
     * @return Weyforth\Connect\TwitterDriver
     */
    protected function createTwitterDriver()
    {
        return new TwitterDriver;
    }


    /**
     * Get the default connect driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->app['config']['connect.driver'];
    }


    /**
     * Get the default connect driver name.
     *
     * @return string
     */
    public function getDriver()
    {
        return $this->getDefaultDriver();
    }


    /**
     * Set the default connect driver.
     *
     * @param string $name Name of driver.
     *
     * @return void
     */
    public function setDefaultDriver($name)
    {
        $this->app['config']['connect.driver'] = $name;
    }


    /**
     * Set the current connect driver.
     *
     * @param string $name Name of driver.
     *
     * @return void
     */
    public function setDriver($name)
    {
        $this->setDefaultDriver($name);
    }


    /**
     * Set the social conenction callback URL.
     *
     * @param string $url Callback URL.
     *
     * @return void
     */
    public function setCallbackURL($url)
    {
        $this->app['config']['connect.callbackURL'] = $url;
    }


    /**
     * Get the social conenction callback URL.
     *
     * @return string
     */
    public function getCallbackURL()
    {
        return $this->app['config']['connect.callbackURL'];
    }


    /**
     * Sets the active driver.
     *
     * The first active driver is found by iterating through
     * the driver array, and kept active.
     *
     * @param array $drivers Array of driver names.
     *
     * @return void
     */
    public function setActiveDriver(array $drivers)
    {
        foreach ($drivers as $driver) {
            $this->setDriver($driver);

            if ($this->isActive()) {
                break;
            }
        }
    }


}
