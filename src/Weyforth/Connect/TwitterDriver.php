<?php
/**
 * Twitter connect driver.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Connect;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class TwitterDriver extends BaseDriver implements DriverInterface
{

    /**
     * The Twitter oauth object.
     *
     * @var TwitterAPI
     */
    protected $api;


    /**
     * Sets up twitter API.
     */
    public function __construct()
    {
        parent::__construct();

        $this->api = App::make('twitter');
    }


    /**
     * {@inheritdoc}
     */
    public function connectURL($authenticate = true)
    {
        $params = array(
            'oauth_callback' => \Connect::getCallbackURL(),
            'x_auth_access_type' => 'write'
        );

        $forceLogin = '';
        if (!$authenticate) {
            $forceLogin = '&force_login=true';
        }

        $type = 'oauth/authenticate';

        $this->api->request(
            'POST',
            $this->api->url('oauth/request_token', ''),
            $params
        );

        if ($this->api->response['code'] == 200) {
            $params = $this->api->extract_params($this->api->response['response']);
            $this->setSessionData($params);
            $url = $this->api->url($type, '').'?oauth_token='.$params['oauth_token'].$forceLogin;

            return $url;
        } else {
            return false;
        }
    }


    /**
     * {@inheritdoc}
     */
    public function reconnect()
    {
        return Redirect::to($this->connectURL(false));
    }


    /**
     * {@inheritdoc}
     */
    public function respond()
    {
        $sessionData = $this->getSessionData();
        if (isset($_REQUEST['oauth_verifier'])
            && isset($_REQUEST['oauth_token'])
            && $sessionData
            && isset($sessionData['oauth_token'])
            && isset($sessionData['oauth_token_secret'])
        ) {
            if ($_REQUEST['oauth_token'] == $sessionData['oauth_token']) {
                $this->api->config['user_token']  = $sessionData['oauth_token'];
                $this->api->config['user_secret'] = $sessionData['oauth_token_secret'];

                $this->api->request(
                    'POST',
                    $this->api->url('oauth/access_token', ''),
                    array(
                        'oauth_verifier' => $_REQUEST['oauth_verifier']
                    )
                );

                if ($this->api->response['code'] == 200) {
                    $this->unsetSessionData();
                    $userData = $this->api->extract_params(
                        $this->api->response['response']
                    );

                    $this->setupAPI($userData);

                    $response = $this->api->getUsers(
                        array(
                            'id' => $userData['user_id'],
                            'screen_name' => $userData['screen_name']
                        )
                    );

                    if ($this->api->response['code'] == 200) {
                        $userData['profile_image_url'] = $response->profile_image_url;
                        $userData['name']              = $response->name;
                    } else {
                    }

                    $this->setSessionData($userData);
                } else {
                }
            } else {
            }
        } else {
            // TODO: Add error handling to Twitter and Facebook drivers.
        }
    }


    /**
     * {@inheritdoc}
     */
    public function disconnect()
    {
        $this->unsetSessionData();
    }


    /**
     * {@inheritdoc}
     */
    public function isActive()
    {
        $sessionData = $this->getSessionData();
        if ($sessionData
            && isset($sessionData['oauth_token'])
            && isset($sessionData['oauth_token_secret'])
            && isset($sessionData['user_id'])
            && isset($sessionData['screen_name'])
        ) {
            return $sessionData;
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function userImage()
    {
        $userData = $this->getSessionData();

        return str_replace('_normal', '_bigger', $userData['profile_image_url']);
    }


    /**
     * {@inheritdoc}
     */
    public function userID()
    {
        $userData = $this->getSessionData();

        return $userData['user_id'];
    }


    /**
     * {@inheritdoc}
     */
    public function userScreenName($extra = true)
    {
        $userData = $this->getSessionData();
        if ($extra) {
            return '@'.$userData['screen_name'];
        }

        return $userData['screen_name'];
    }


    /**
     * {@inheritdoc}
     */
    public function userName()
    {
        $userData = $this->getSessionData();

        return $userData['name'];
    }


    /**
     * {@inheritdoc}
     */
    public function icon()
    {
        return '&#xf099;';
    }


    /**
     * {@inheritdoc}
     */
    protected function setupAPI($data)
    {
        $this->api->config['user_token']  = $data['oauth_token'];
        $this->api->config['user_secret'] = $data['oauth_token_secret'];
    }


    /**
     * {@inheritdoc}
     */
    public function hasPermissions()
    {
        if ($data = $this->isActive()) {
            $this->setupAPI($data);

            $response = $this->api->getCredentials();
            if ($this->api->response['code'] == 200) {
                return $response;
            }
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function postMessage($message)
    {
        if ($this->hasPermissions()) {
            $response = $this->api->postTweet(
                array(
                    'status' => $message
                )
            );

            if ($this->api->response['code'] == 200) {
                return array(
                    'id' => $response->id,
                    'user' => $response->user->id
                );
            }
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function follow($info)
    {
    }


    /**
     * {@inheritdoc}
     */
    public function removeMessage($id)
    {
        if ($this->hasPermissions()) {
            $response = $this->api->destroyTweet(
                $id,
                array(
                    'trim_user' => true
                )
            );

            if ($this->api->response['code'] == 200) {
                return true;
            }
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function verb()
    {
        return 'tweet';
    }


}
