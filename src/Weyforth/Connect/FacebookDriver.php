<?php
/**
 * Facebook connect driver.
 *
 * @author    Mike Farrow <contact@mikefarrow.co.uk>
 * @license   Proprietary/Closed Source
 * @copyright Mike Farrow
 */

namespace Weyforth\Connect;

use Thomaswelton\LaravelFacebook\Facades\Facebook as FacebookAPI;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;

class FacebookDriver extends BaseDriver implements DriverInterface
{


    /**
     * {@inheritdoc}
     */
    public function connectURL($authenticate = true)
    {
        $params = array(
            'scope' => 'publish_actions',
            'redirect_uri' => \Connect::getCallbackURL()
        );

        return FacebookAPI::getLoginUrl($params);
    }


    /**
     * {@inheritdoc}
     */
    public function reconnect()
    {
        $params = array(
            'next' => $this->connectURL()
        );

        return Redirect::to(FacebookAPI::getLogoutUrl($params));
    }


    /**
     * {@inheritdoc}
     */
    public function respond()
    {
        // Need to call something on api otherwise we will lose our session.
        FacebookAPI::getUser();

        try {
            $response = FacebookAPI::api('/me', 'get');
            $this->setSessionData(
                array(
                    'name' => $response['name'],
                    'username' => array_key_exists('username', $response) ?
                        $response['username'] :
                        $response['id']
                )
            );
        } catch (FacebookApiException $e) {

        }
    }


    /**
     * {@inheritdoc}
     */
    public function disconnect()
    {
        FacebookAPI::destroySession();
        $this->unsetSessionData();

        // Need to provide "login with different user" link.
    }


    /**
     * {@inheritdoc}
     */
    public function isActive()
    {
        $userData = $this->getSessionData();
        if (FacebookAPI::getUser() != 0 && $userData) {
            return true;
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function userImage()
    {
        return 'https://graph.facebook.com/'.FacebookAPI::getUser().'/picture';
    }


    /**
     * {@inheritdoc}
     */
    public function userID()
    {
        return FacebookAPI::getUser();
    }


    /**
     * {@inheritdoc}
     */
    public function userScreenName($extra = true)
    {
        $userData = $this->getSessionData();
        if ($extra) {
            return '/'.$userData['username'];
        }

        return $userData['username'];
    }


    /**
     * {@inheritdoc}
     */
    public function userName()
    {
        $userData = $this->getSessionData();

        return $userData['name'];
    }


    /**
     * {@inheritdoc}
     */
    public function icon()
    {
        return '&#xf09a;';
    }


    /**
     * {@inheritdoc}
     */
    public function hasPermissions()
    {
        if ($this->isActive()) {
            $permissions = FacebookAPI::api('/me/permissions');

            if (isset($permissions['data'][0])
                && array_key_exists('publish_actions', $permissions['data'][0])
                && $permissions['data'][0]['publish_actions'] == 1
            ) {
                $data = FacebookAPI::api('/me');

                return $data;
            }
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function postMessage($message)
    {
        if ($this->hasPermissions()) {
            $data = array(
                'message' => $message,
            );

            $postResult = FacebookAPI::api(
                '/me/feed',
                'post',
                $data
            );
            if (FacebookAPI::getUser() != 0 && $postResult['id']) {
                return array(
                    'id' => $postResult['id'],
                    'user' => FacebookAPI::getUser()
                );
            } else {
                throw new Exception('Unexpected response');
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function follow($info)
    {
        /*
        if($this->hasPermissions()){
            $response = FacebookAPI::api(
                "/me/og.likes",
                "POST",
                array (
                    'object' => $info,
                )
            );
        }*/
    }


    /**
     * {@inheritdoc}
     */
    public function removeMessage($id)
    {
        if ($this->hasPermissions()) {
            $data = array(
                'method' => 'delete',
            );
            try {
                $postResult = FacebookAPI::api($id, 'delete');

            } catch (FacebookApiException $e) {
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function verb()
    {
        return 'post';
    }


}
